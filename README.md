# Ansible execution environments builder

This repo contains definition files and a GitLab CI pipeline to automate the building and publishing of custom Ansible execution environments (EE) using [ansible-builder](https://github.com/ansible/ansible-builder).

Full [documentation](https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.0-ea/html-single/ansible_builder_guide/index) on ansible-builder is available from Redhat.  

## Requirements

This document expects that you are working with a Linux terminal with the following installed:

* [podman](https://podman.io/) OR [docker](https://www.docker.com/)
* [ansible-builder](https://ansible-builder.readthedocs.io/en/stable/)

You will also need access to a container registry to publish the EE to.

## Conventions

The directories in this repository should follow the format that `ansible-builder` expects.  
See [example-ee](example-ee).  

Each EE directory will have the following files.

* `bindep.txt` for any OS packages to be installed (See the bindep docs for info on format)
* `requirements.txt` for any Python packages to be installed with pip
* `requirements.yml` for any Ansible roles or collections to be installed with ansible-galaxy

## New Execution Environment

* Copy `example_ee` directory to a new directory

  ```bash
  cp -a example-ee new-ee
  cd new-ee
  #tune requirements.txt,requirements.yml or bindep.txt
  # replace $REGISTRY with your container registry address
  ansible-builder build -t $REGISTRY/new-ee:latest
  # test locally with ansible-navigator
  # publish when happy
  podman --events-backend=file push $REGISTRY/new-ee:latest
  ```

## GitLab CI pipeline

There is a pipeline already setup in `.gitlab-ci.yml`.  

### Docker in Docker or Podman

You have a choice of using DIND or Podman for building the EE by changing `extends: .build_dind` to `extends: .build_podman` in `.gitlab-ci.yml`

```yaml
build-images:
  extends: .build_dind
  parallel:
    matrix:
```

### Adding or Removing EE to/from the Pipeline

For each EE directory you have created or removed, add or remove an entry to the `matrix` list in `.gitlab-ci.yml`

```yaml
  parallel:
    matrix:
      - DIRECTORY: 'example_ee'
      - DIRECTORY: 'example2_ee'
```

## LICENSE

Copyright (c) 2022 Contojo Pty Ltd

This repository is distributed under the MIT License excluding portions which are copied from other sources.  
See the accompanying [LICENSE](LICENSE) file for a copy of the license or [choosealicense.com](https://choosealicense.com/licenses/mit)

## Useful documentation and links

* [ansible-navigator](https://github.com/ansible/ansible-navigator)
* [ansible-builder](https://github.com/ansible/ansible-builder)
* AWX's default EE [definitions](https://github.com/ansible/awx-ee/tree/devel/_build)
